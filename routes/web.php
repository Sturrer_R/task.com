<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'ArticleController@index')->name('index');
    Route::get('/edit', 'ArticleController@create')->name('edit');
    Route::post('/', 'ArticleController@store');
    Route::get('/{article}', 'ArticleController@show');
    Route::get('/edit/{article}', 'ArticleController@edit');
    Route::put('/{article}', 'ArticleController@update')->name('update');
    Route::delete('/delete/{article}', 'ArticleController@destroy')->name('delete');


});



