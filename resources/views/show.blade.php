@extends('welcome')
@section('content')
    @include('layouts.app')
    <h3>Showing Ad {{$article->title}}</h3>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Title:</strong>
                {{ $article->title}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Description:</strong>
                {{ $article->description}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <a class="btn btn-primary" href="{{url('/')}}"> Back</a>
            @if(Auth::user()->id == $article->author_id)
                <form action="{{url('delete',$article->id)}}" method="POST" onsubmit="return confirmDelete()">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="submit" class="btn btn-danger" value="Delete"/>
                </form>
                <script>
                    function confirmDelete() {
                        return confirm("Are you sure?");
                    }
                </script>
            @endif

        </div>
    </div>
@endsection