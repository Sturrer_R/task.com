@extends('welcome')
@section('content')
    @include('layouts.app')
    <h1>Edit Add</h1>
    <hr>
    <form action="{{route('update', $article->id)}}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Ad Title</label>
            <input type="text" value="{{$article->title}}" class="form-control" id="articleTitle" name="title">
        </div>
        <div class="form-group">
            <label for="description">Addd Description</label>
            <input type="text" value="{{$article->description}}" class="form-control" id="articleDescription"
                   name="description">
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Save</button>

    </form>
    @if(Auth::user()->id == $article->author_id)
        <form action="{{url('delete',$article->id)}}" method="POST" onsubmit="return confirmDelete()">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn btn-danger" value="Delete"/>
        </form>
        <script>
            function confirmDelete() {
                return confirm("Are you sure?");
            }
        </script>
    @endif
@endsection