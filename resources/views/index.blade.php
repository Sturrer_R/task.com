@extends('welcome')
@section('content')
    @include('layouts.app')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Ad Title</th>
            <th scope="col">Ad Description</th>
            <th scope="col">Author name</th>
            <th scope="col">Created At</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <th scope="row">{{$article->id}}</th>
                <td><a href="/{{$article->id}}">{{$article->title}}</a></td>
                <td>{{$article->description}}</td>
                <td>{{$article->user->username}}</td>
                <td>{{$article->created_at->toFormattedDateString()}}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <a href="{{url('edit',$article->id)}} ">
                            <button type="button" class="btn btn-warning">Edit</button>
                        </a>&nbsp;
                        @if(Auth::user()->id == $article->author_id)
                            <form action="{{url('delete',$article->id)}}" method="POST"
                                  onsubmit="return confirmDelete()">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-danger" value="Delete"/>
                            </form>
                            <script>
                                function confirmDelete() {
                                    return confirm("Are you sure?");
                                }
                            </script>
                        @endif
                    </div>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
    {{$articles->render()}}
@endsection
