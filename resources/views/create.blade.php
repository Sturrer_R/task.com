@extends('welcome')
@section('content')
    @include('layouts.app')
    <h1>Add New Ad</h1>
    <hr>
    <form action="{{route('index')}}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">AdTitle</label>
            <input type="text" class="form-control" id="ArticleTitle" name="title">
        </div>
        <div class="form-group">
            <label for="description">Ad Description</label>
            <input type="text" class="form-control" id="ArticleDescription" name="description">
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Create</button>

    </form>
@endsection