<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Auth;

class ArticleController extends Controller
{
    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $articles = Article::paginate(5);
        return view('index', compact('articles'));

    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     *
     * @param Article $article
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Article $article)
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        Article::create([
            'title' => $request->title,
            'description' => $request->description,
            'author_id' => Auth::user()->id
        ]);

        return redirect('/');


    }

    /**
     *
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $article = Article::find($id);
        if (!$article) {
            $request->session()->flash('message', 'Article #' . $id . ' not found');
            return redirect()->route('index');
        }
        return view('show', compact('article'));

    }


    /**
     *
     * @param \App\Article
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $article = Article::find($id);
        if (!$article) {
            $request->session()->flash('message', 'Article #' . $id . ' not found');
            return redirect()->route('index');
        }

        return view('edit', compact('article', $article));
    }

    /**
     *
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @param \App\Article
     */
    public function update(Request $request, Article $article)
    {

        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);


        $article->title = $request->title;
        $article->description = $request->description;
        $article->save();
        $request->session()->flash('message', 'Successfully modified the article!');
        return redirect()->route('index');

    }

    /**
     *
     * @param int $id
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request, $id)
    {
        Article::destroy($id);
        $request->session()->flash('message', 'Successfully deleted the article!');
        return redirect()->route('index');
    }

}
