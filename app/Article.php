<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title',
        'description',
        'author_id'
    ];

    function user(){
        return $this->belongsTo('App\User', 'author_id', 'id');
    }
}
